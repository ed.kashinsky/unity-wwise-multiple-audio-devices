/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAYTROPICALAMBIENCE = 1248747267U;
    } // namespace EVENTS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN_SOUNDBANK = 2228651116U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID ADDITIONAL_HEADPHONES_1_BUS = 1301761527U;
        static const AkUniqueID ADDITIONAL_HEADPHONES_2_BUS = 922935528U;
        static const AkUniqueID ADDITIONAL_HEADPHONES_3_BUS = 2612241173U;
        static const AkUniqueID DEFAULT_HEADPHONES_BUS = 3273377481U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID ADDITIONAL_HEADPHONES_1_AUX = 1032775159U;
        static const AkUniqueID ADDITIONAL_HEADPHONES_2_AUX = 654934856U;
        static const AkUniqueID ADDITIONAL_HEADPHONES_3_AUX = 2880830293U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID ADDITIONAL_HEADPHONES_1 = 3563127276U;
        static const AkUniqueID ADDITIONAL_HEADPHONES_2 = 3563127279U;
        static const AkUniqueID ADDITIONAL_HEADPHONES_3 = 3563127278U;
        static const AkUniqueID DEFAULT_HEADPHONES = 3736012306U;
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__

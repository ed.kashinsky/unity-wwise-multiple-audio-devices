#if ! (UNITY_DASHBOARD_WIDGET || UNITY_WEBPLAYER || UNITY_WII || UNITY_WIIU || UNITY_NACL || UNITY_FLASH || UNITY_BLACKBERRY) // Disable under unsupported platforms.
//////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2017 Audiokinetic Inc. / All Rights Reserved
//
//////////////////////////////////////////////////////////////////////

[UnityEngine.AddComponentMenu("Wwise/AkAudioListener")]
[UnityEngine.RequireComponent(typeof(AkGameObj))]
[UnityEngine.DisallowMultipleComponent]
[UnityEngine.DefaultExecutionOrder(-50)]
///@brief Add this script on the game object that represent a listener.  This is normally added to the Camera object or the Player object, but can be added to any game object when implementing 3D busses.  \c isDefaultListener determines whether the game object will be considered a default listener - a listener that automatically listens to all game objects that do not have listeners attached to their AkGameObjListenerList's.
/// \sa
/// - <a href="https://www.audiokinetic.com/library/edge/?source=SDK&id=soundengine__listeners.html" target="_blank">Integrating Listeners</a> (Note: This is described in the Wwise SDK documentation.)
public class AkAudioListener : UnityEngine.MonoBehaviour
{
	private static readonly DefaultListenerList defaultListeners = new DefaultListenerList();
	private ulong akGameObjectID = AkSoundEngine.AK_INVALID_GAME_OBJECT;
	private System.Collections.Generic.List<AkGameObj> EmittersToStartListeningTo = 
		new System.Collections.Generic.List<AkGameObj>();
	private System.Collections.Generic.List<AkGameObj> EmittersToStopListeningTo = 
		new System.Collections.Generic.List<AkGameObj>();

	public bool isDefaultListener = true;

	public string AudioDeviceName = "";
	public string WwiseAudioDevice = "";

	private string DefaultWwiseAudioDevice = "Default_Headphones";
	private uint uDeviceId = AkSoundEngine.AK_INVALID_DEVICE_ID;
	private ulong out_DeviceId = AkSoundEngine.AK_INVALID_OUTPUT_DEVICE_ID;
	private static System.Collections.Generic.List<uint> UsingAudioDeviceIds;

	public static DefaultListenerList DefaultListeners
	{
		get { return defaultListeners; }
	}

	public void StartListeningToEmitter(AkGameObj emitter)
	{
		EmittersToStartListeningTo.Add(emitter);
		EmittersToStopListeningTo.Remove(emitter);
	}

	public void StopListeningToEmitter(AkGameObj emitter)
	{
		EmittersToStartListeningTo.Remove(emitter);
		EmittersToStopListeningTo.Add(emitter);
	}

	public void SetIsDefaultListener(bool isDefault)
	{
		if (isDefaultListener != isDefault)
		{
			isDefaultListener = isDefault;

			if (isDefault)
				DefaultListeners.Add(this);
			else
				DefaultListeners.Remove(this);
		}
	}

	private void Awake()
	{
		var akGameObj = GetComponent<AkGameObj>();
		UnityEngine.Debug.Assert(akGameObj != null);
		if (akGameObj)
			akGameObj.Register();

		akGameObjectID = AkSoundEngine.GetAkGameObjectID(gameObject);
	}

	private void ConnectToAudioDevice()
	{
		uint countActiveDevices = AkSoundEngine.GetWindowsDeviceCount(AkAudioDeviceState.AkDeviceState_Active);

		if (UsingAudioDeviceIds == null)
		{
			UsingAudioDeviceIds = new System.Collections.Generic.List<uint>();
		}

		if (WwiseAudioDevice == DefaultWwiseAudioDevice)
		{
			// add output for default audio device
			var defaultAudioDevice = AkSoundEngine.GetWindowsDeviceName(0, out uDeviceId, AkAudioDeviceState.AkDeviceState_Active);
			AddCustomOutput(defaultAudioDevice);
			return;
		}
		else
		{
			// search of available audio device
			// (0 is default device)
			for (uint i = 1; i < countActiveDevices; i++)
			{
				bool hasFound = false;
				var deviceName = AkSoundEngine.GetWindowsDeviceName((int)i, out uDeviceId, AkAudioDeviceState.AkDeviceState_Active);

				if (UsingAudioDeviceIds.Contains(uDeviceId))
				{
					continue;
				}

				if (AudioDeviceName.Length > 0)
				{
					hasFound = deviceName.Contains(AudioDeviceName);
				}
				else
				{
					hasFound = true;
				}

				// finally!!
				if (hasFound)
				{
					if (AddCustomOutput(deviceName) == AKRESULT.AK_Success)
					{
						// remove default audio device
						var outputId = AkSoundEngine.GetOutputID((new AkOutputSettings("Default_Headphones", 0)).audioDeviceShareset, 0);
						AkSoundEngine.RemoveOutput(outputId);
					}

					return;
				}
			}
		}

		if (AudioDeviceName.Length > 0)
		{
			UnityEngine.Debug.LogError("WwiseUnity: <b>" + AudioDeviceName + "</b> not found in \"" + gameObject.name + "\". Please check AkAudioListerner settings.");
		}
		else
		{
			UnityEngine.Debug.LogError("WwiseUnity: No available audio devices found for \"" + gameObject.name + "\". Please check AkAudioListerner settings.");
		}
	}

	private void DisconnectAudioDevice()
	{
		if (out_DeviceId != AkSoundEngine.AK_INVALID_OUTPUT_DEVICE_ID && WwiseAudioDevice != DefaultWwiseAudioDevice)
		{
			AkSoundEngine.RemoveOutput(out_DeviceId);

			UnityEngine.Debug.Log(string.Format("WwiseUnity: <b>{0}</b> ({1}) is disconnected.", WwiseAudioDevice, uDeviceId));

			UsingAudioDeviceIds.RemoveAll((target) => { return target == uDeviceId; });

			uDeviceId = AkSoundEngine.AK_INVALID_DEVICE_ID;
			out_DeviceId = AkSoundEngine.AK_INVALID_OUTPUT_DEVICE_ID;
		}
	}

	private AKRESULT AddCustomOutput(string DeviceName)
	{
		ulong[] listeners = { akGameObjectID };

		if (WwiseAudioDevice.Length == 0)
		{
			WwiseAudioDevice = "System";
		}

		var res = AkSoundEngine.AddOutput(
			new AkOutputSettings(WwiseAudioDevice, uDeviceId),
			out out_DeviceId,
			listeners,
			1
		);

		if (res == AKRESULT.AK_Success)
		{
			UnityEngine.Debug.Log(string.Format("WwiseUnity: <b>{0}</b> and \"{1}\" is connected in \"{2}\" ({3}).", DeviceName, WwiseAudioDevice, gameObject.name, uDeviceId));
			UsingAudioDeviceIds.Add(uDeviceId);
		}
		else
		{
			UnityEngine.Debug.Log(string.Format("WwiseUnity: Problem with connection between <b>{0}</b> and \"{1}\" in \"{2}\" (Error: {3}).", DeviceName, WwiseAudioDevice, gameObject.name, res));
		}

		return res;
	}

	private void OnEnable()
	{
		if (isDefaultListener)
			DefaultListeners.Add(this);

		ConnectToAudioDevice();
	}

	private void OnDisable()
	{
		if (isDefaultListener)
			DefaultListeners.Remove(this);

		DisconnectAudioDevice();
	}

	private void Update()
	{
		for (var i = 0; i < EmittersToStartListeningTo.Count; ++i)
			EmittersToStartListeningTo[i].AddListener(this);
		EmittersToStartListeningTo.Clear();

		for (var i = 0; i < EmittersToStopListeningTo.Count; ++i)
			EmittersToStopListeningTo[i].RemoveListener(this);
		EmittersToStopListeningTo.Clear();
	}

	public ulong GetAkGameObjectID()
	{
		return akGameObjectID;
	}

	public class BaseListenerList
	{
		// @todo: Use HashSet<ulong> and CopyTo() with a private ulong[]
		private readonly System.Collections.Generic.List<ulong> listenerIdList = new System.Collections.Generic.List<ulong>();

		private readonly System.Collections.Generic.List<AkAudioListener> listenerList =
			new System.Collections.Generic.List<AkAudioListener>();

		public System.Collections.Generic.List<AkAudioListener> ListenerList
		{
			get { return listenerList; }
		}

		/// <summary>
		///     Uniquely adds listeners to the list
		/// </summary>
		/// <param name="listener"></param>
		/// <returns></returns>
		public virtual bool Add(AkAudioListener listener)
		{
			if (listener == null)
				return false;

			var gameObjectId = listener.GetAkGameObjectID();
			if (listenerIdList.Contains(gameObjectId))
				return false;

			listenerIdList.Add(gameObjectId);
			listenerList.Add(listener);
			return true;
		}

		/// <summary>
		///     Removes listeners from the list
		/// </summary>
		/// <param name="listener"></param>
		/// <returns></returns>
		public virtual bool Remove(AkAudioListener listener)
		{
			if (listener == null)
				return false;

			var gameObjectId = listener.GetAkGameObjectID();
			if (!listenerIdList.Remove(gameObjectId))
				return false;

			listenerList.Remove(listener);
			return true;
		}

		public ulong[] GetListenerIds()
		{
			return listenerIdList.ToArray();
		}
	}

	public class DefaultListenerList : BaseListenerList
	{
		public override bool Add(AkAudioListener listener)
		{
			var ret = base.Add(listener);
			if (ret && AkSoundEngine.IsInitialized())
				AkSoundEngine.AddDefaultListener(listener.gameObject);
			return ret;
		}

		public override bool Remove(AkAudioListener listener)
		{
			var ret = base.Remove(listener);
			if (ret && AkSoundEngine.IsInitialized())
				AkSoundEngine.RemoveDefaultListener(listener.gameObject);
			return ret;
		}
	}

	#region WwiseMigration

#pragma warning disable 0414 // private field assigned but not used.

	[UnityEngine.SerializeField]
	// Wwise v2016.2 and below supported up to 8 listeners[0-7].
	public int listenerId = 0;

#pragma warning restore 0414 // private field assigned but not used.

	public void Migrate14()
	{
		var wasDefaultListener = listenerId == 0;
		UnityEngine.Debug.Log("WwiseUnity: AkAudioListener.Migrate14 for " + gameObject.name);
		isDefaultListener = wasDefaultListener;
	}

	#endregion
}
#endif // #if ! (UNITY_DASHBOARD_WIDGET || UNITY_WEBPLAYER || UNITY_WII || UNITY_WIIU || UNITY_NACL || UNITY_FLASH || UNITY_BLACKBERRY) // Disable under unsupported platforms.
Audio Bus	ID	Name			Wwise Object Path	Notes
	922935528	Additional_Headphones_2_Bus			\Default Work Unit\Additional_Headphones_2_Bus	
	1301761527	Additional_Headphones_1_Bus			\Default Work Unit\Additional_Headphones_1_Bus	
	2612241173	Additional_Headphones_3_Bus			\Default Work Unit\Additional_Headphones_3_Bus	
	3273377481	Default_Headphones_Bus			\Default Work Unit\Default_Headphones_Bus	

Auxiliary Bus	ID	Name			Wwise Object Path	Notes
	654934856	Additional_Headphones_2_Aux			\Default Work Unit\Additional_Headphones_2_Bus\Additional_Headphones_2_Aux	
	1032775159	Additional_Headphones_1_Aux			\Default Work Unit\Additional_Headphones_1_Bus\Additional_Headphones_1_Aux	
	2880830293	Additional_Headphones_3_Aux			\Default Work Unit\Additional_Headphones_3_Bus\Additional_Headphones_3_Aux	

Audio Devices	ID	Name	Type				Notes
	2317455096	No_Output	No Output			
	3563127276	Additional_Headphones_1	System			
	3563127278	Additional_Headphones_3	System			
	3563127279	Additional_Headphones_2	System			
	3736012306	Default_Headphones	System			
	3859886410	System	System			

